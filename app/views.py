from django.http import Http404
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from .models import Users
from rest_framework import viewsets
from .models import UserPost
from .serializers import UserSerializers, UserPostSerializers


class UsersList(APIView):

    def get(self, request):
        User_details = Users.objects.all()
        serializer = UserSerializers(User_details, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = UserSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(APIView):
    def get_object(self, pk):
        try:
            return Users.objects.get(pk=pk)
        except Users.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        snippet = self.get_object(pk)
        serializer = UserSerializers(snippet)
        return Response(serializer.data)

class UserViewSet(viewsets.ModelViewSet):
    queryset = Users.objects.all()
    serializer_class = UserSerializers

class UserPostViewSet(viewsets.ModelViewSet):
    queryset = UserPost.objects.all()
    serializer_class = UserPostSerializers

    # @action(methods=['put'], detail=False)
    # def filter_by_something(self, request):
    #     something = request.data.get('that_something')
    #     queryset = Users.objects.filter(something__exact=something)
    #     return Response(self.get_serializer(queryset, many=True).data)

    # ***********User post code starts from here**********


class UsersPostList(APIView):

    def get(self, request):
        Userpost_details = UserPost.objects.all()
        serializer = UserPostSerializers(Userpost_details, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = UserPostSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserPostDetail(APIView):
    def get_object(self, pk):
        try:
            return UserPost.objects.get(pk=pk)
        except UserPost.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        snippet = self.get_object(pk)
        serializer = UserPostSerializers(snippet)
        return Response(serializer.data)




