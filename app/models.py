from django.db import models


class Users(models.Model):
    user_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    profile_name = models.CharField(max_length=30)
    profile_picture = models.CharField(max_length=1000)
    phone = models.CharField(max_length=15)
    email = models.EmailField()
    password = models.CharField(max_length=25, default='')

    def __str__(self):
        return self.name


class UserPost(models.Model):
    post_id = models.AutoField(primary_key=True)
    user = models.ForeignKey('Users', related_name='userpost', on_delete=models.CASCADE)
    recipe_name = models.CharField(max_length=100)
    recipe_image = models.CharField(max_length=1000)
    recipe_ingredients = models.CharField(max_length=1000)
    recipe_direction = models.CharField(max_length=1000)
    recipe_cuisine = models.CharField(max_length=30)
    recipe_calories = models.CharField(max_length=20)
    recipe_mealtype = models.CharField(max_length=30)
    recipe_preptime = models.CharField(max_length=20)

    def __str__(self):
        return self.recipe_name