from rest_framework import serializers
from .models import Users
from .models import UserPost


class UserSerializers(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = '__all__'


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Users
        fields = '__all__'


class UserPostSerializers(serializers.ModelSerializer):
    class Meta:
        model = UserPost
        fields = '__all__'
