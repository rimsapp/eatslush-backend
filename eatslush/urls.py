from django.urls import include, path
from rest_framework import routers
from app import views

router = routers.DefaultRouter()
router.register(r'users', views.UserViewSet)
router.register(r'posts', views.UserPostViewSet)

urlpatterns = [
    path('', include(router.urls)),]